﻿using StockroomManagement.DataAccess;
using StockroomManagement.Repository.Interfaces;

namespace StockroomManagement.Repository.Repositories
{
    public class StorageRepository : IStorageRepository
    {
        private StockroomContext context;

        public StorageRepository(StockroomContext context)
        {
            this.context = context;
        }
    }
}
