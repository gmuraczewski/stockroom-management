﻿namespace StockroomManagement.BusinessLogic.Models
{
    public class AvailabilityDto
    {
        public int Id { get; set; }

        public int ProductId { get; set; }

        public int StorageId { get; set; }

        public int Quantity { get; set; }
    }
}
