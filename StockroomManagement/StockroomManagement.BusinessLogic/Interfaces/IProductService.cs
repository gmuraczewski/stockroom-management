﻿using System.Collections.Generic;
using StockroomManagement.BusinessLogic.Models;

namespace StockroomManagement.BusinessLogic.Interfaces
{
    public interface IProductService
    {
        void AddProduct(ProductDto productDto);

        ProductDto GetProduct(int id);

        void EditProduct(ProductDto productDto);

        List<ProductDto> GetProductList();

        void DeleteProduct(int id);
    }
}
