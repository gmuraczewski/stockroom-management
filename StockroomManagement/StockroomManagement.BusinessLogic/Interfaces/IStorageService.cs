﻿using StockroomManagement.BusinessLogic.Models;
using System.Collections.Generic;

namespace StockroomManagement.BusinessLogic.Interfaces
{
    public interface IStorageService
    {
        void Add(StorageDto storageDto);

        void Edit(StorageDto storageDto);

        List<StorageDto> GetStorageList();

        StorageDto GetStorage(int id);
    }
}
