﻿using AutoMapper;
using StockroomManagement.BusinessLogic.Interfaces;
using StockroomManagement.BusinessLogic.Mappers;
using StockroomManagement.BusinessLogic.Models;
using StockroomManagement.DataAccess.Interfaces;
using StockroomManagement.DataAccess.Models;
using System.Collections.Generic;
using System.Linq;

namespace StockroomManagement.BusinessLogic.Services
{
    public class ProductService : IProductService
    {
        private IRepository<Product> repository;

        public ProductService(IRepository<Product> repository)
        {
            this.repository = repository;
            ProductModelMapper.CreateMapping();
        }

        public void AddProduct(ProductDto productDto)
        { 
            Product product = Mapper.Map<ProductDto, Product>(productDto);
            repository.Add(product);
        }

        public void EditProduct(ProductDto productDto)
        {
            Product product = Mapper.Map<ProductDto, Product>(productDto);
            repository.Update(product);
        }

        public ProductDto GetProduct(int id)
        {
            Product product = repository.Query().Where(prod => prod.ID == id).FirstOrDefault();
            return Mapper.Map<Product, ProductDto>(product);
        }
    
        public List<ProductDto> GetProductList()
        {
            List<Product> productList = repository.Query().OrderBy(prod => prod.Name).ToList();
            return Mapper.Map<List<Product>, List<ProductDto>>(productList);
        }

        public void DeleteProduct(int id)
        {
            ProductDto productDto = GetProduct(id);
            Product product = Mapper.Map<ProductDto, Product>(productDto);
            repository.Delete(product);
        }
    }
}
