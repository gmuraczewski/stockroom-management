﻿using AutoMapper;
using StockroomManagement.BusinessLogic.Models;
using StockroomManagement.DataAccess.Models;

namespace StockroomManagement.BusinessLogic.Mappers
{
    public static class AvailabilityModelMapper
    {
        public static void CreateMapping()
        {
            Mapper.CreateMap<Availability, AvailabilityDto>()
                .ReverseMap();
        }
    }
}
