﻿using AutoMapper;
using StockroomManagement.BusinessLogic.Models;
using StockroomManagement.DataAccess.Models;

namespace StockroomManagement.BusinessLogic.Mappers
{
    public static class ProductModelMapper
    {
        public static void CreateMapping()
        {
            Mapper.CreateMap<Product, ProductDto>()
                .ForMember(dest => dest.Id, opts => opts.MapFrom(src => src.ID))
                .ForMember(dest => dest.Name, opts => opts.MapFrom(src => src.Name))
                .ForMember(dest => dest.Description, opts => opts.MapFrom(src => src.Description))
                .ForMember(dest => dest.Price, opts => opts.MapFrom(src => src.Price))
                .ReverseMap()
                .ForMember(dest => dest.Name, opts => opts.MapFrom(src => src.Name))
                .ForMember(dest => dest.Description, opts => opts.MapFrom(src => src.Description))
                .ForMember(dest => dest.Price, opts => opts.MapFrom(src => src.Price))
                .ForMember(dest => dest.ID, opts => opts.MapFrom(src => src.Id));
                }
    }
}
