﻿using AutoMapper;
using StockroomManagement.BusinessLogic.Models;
using StockroomManagement.DataAccess.Models;

namespace StockroomManagement.BusinessLogic.Mappers
{
    public static class StorageModelMapper
    {
        public static void CreateMapping()
        {
            Mapper.CreateMap<Storage, StorageDto>()
                .ReverseMap();
        }
    }
}
