﻿using AutoMapper;
using StockroomManagement.BusinessLogic.Models;
using StockroomManagement.Models.Storage;

namespace StockroomManagement.Mappers
{
    public class StorageViewModelMapper
    {
        public void CreateMapping()
        {
            Mapper.CreateMap<StorageDto, AddStorageViewModel>();

            Mapper.CreateMap<StorageDto, EditStorageViewModel>();

            Mapper.CreateMap<StorageDto, StorageListViewModel>();
        }
    }
}