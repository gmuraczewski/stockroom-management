﻿using AutoMapper;
using StockroomManagement.BusinessLogic.Models;
using StockroomManagement.Models.Product;

namespace StockroomManagement.Mappers
{
    public static class ProductViewModelMapper
    {
        public static void CreateMapping()
        {
            Mapper.CreateMap<ProductDto, AddProductViewModel>()
                .ForMember(dest => dest.Name, opts => opts.MapFrom(src => src.Name))
                .ForMember(dest => dest.Description, opts => opts.MapFrom(src => src.Description))
                .ForMember(dest => dest.Price, opts => opts.MapFrom(src => src.Price))
                .ReverseMap()
                .ForMember(dest => dest.Name, opts => opts.MapFrom(src => src.Name))
                .ForMember(dest => dest.Description, opts => opts.MapFrom(src => src.Description))
                .ForMember(dest => dest.Price, opts => opts.MapFrom(src => src.Price));

            Mapper.CreateMap<ProductDto, EditProductViewModel>()    
                .ForMember(dest => dest.Name, opts => opts.MapFrom(src => src.Name))
                .ForMember(dest => dest.Description, opts => opts.MapFrom(src => src.Description))
                .ForMember(dest => dest.Price, opts => opts.MapFrom(src => src.Price))
                .ReverseMap()
                .ForMember(dest => dest.Name, opts => opts.MapFrom(src => src.Name))
                .ForMember(dest => dest.Description, opts => opts.MapFrom(src => src.Description))
                .ForMember(dest => dest.Price, opts => opts.MapFrom(src => src.Price))
                .ForMember(dest => dest.Id, opts => opts.MapFrom(src => src.Id));

            Mapper.CreateMap<ProductDto, ProductListViewModel>()
                .ForMember(dest => dest.Name, opts => opts.MapFrom(src => src.Name))
                .ForMember(dest => dest.Price, opts => opts.MapFrom(src => src.Price))
                .ForMember(dest => dest.Id, opts => opts.MapFrom(src => src.Id));

            Mapper.CreateMap<ProductDto, ProductDetailsViewModel>()
                .ForMember(dest => dest.Name, opts => opts.MapFrom(src => src.Name))
                .ForMember(dest => dest.Description, opts => opts.MapFrom(src => src.Description))
                .ForMember(dest => dest.Price, opts => opts.MapFrom(src => src.Price));
        }
    }
}