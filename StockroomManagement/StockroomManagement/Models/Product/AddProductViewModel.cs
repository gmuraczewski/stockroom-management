﻿namespace StockroomManagement.Models.Product
{
    public class AddProductViewModel
    {
        public string Name { get; set; }

        public double Price { get; set; }

        public string Description { get; set; }
    }
}