﻿namespace StockroomManagement.Models.Product
{
    public class EditProductViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public double Price { get; set; }

        public string Description { get; set; }
    }
}