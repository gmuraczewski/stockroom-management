﻿using StockroomManagement.BusinessLogic.Models;
using System.Collections.Generic;

namespace StockroomManagement.Models.Product
{
    public class ProductListViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public double Price { get; set; }
    }
}