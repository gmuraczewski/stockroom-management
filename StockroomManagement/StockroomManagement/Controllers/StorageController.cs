﻿using StockroomManagement.BusinessLogic.Interfaces;
using StockroomManagement.BusinessLogic.Models;
using System.Web.Mvc;

namespace StockroomManagement.Controllers
{
    public class StorageController : Controller
    {
        private IStorageService storageService;

        public StorageController(IStorageService storageService)
        {
            this.storageService = storageService;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Add(StorageDto storage)
        {
            if(!ModelState.IsValid)
            {
                return View(storage);
            }

            storageService.Add(storage);

            return RedirectToAction("List");
        }
    }
}