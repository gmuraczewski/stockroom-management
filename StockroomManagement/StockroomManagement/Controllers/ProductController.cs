﻿using AutoMapper;
using StockroomManagement.BusinessLogic.Interfaces;
using StockroomManagement.BusinessLogic.Models;
using StockroomManagement.Mappers;
using StockroomManagement.Models.Product;
using System.Collections.Generic;
using System.Web.Mvc;

namespace StockroomManagement.Controllers
{
    public class ProductController : Controller
    {
        private IProductService productService;

        public ProductController(IProductService productService)
        {
            this.productService = productService;
            ProductViewModelMapper.CreateMapping();
        }      

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(AddProductViewModel addProductViewModel)
        {
            if(!ModelState.IsValid)
            {
                return View(addProductViewModel);
            }

            ProductDto productDto = Mapper.Map<AddProductViewModel, ProductDto>(addProductViewModel);
            productService.AddProduct(productDto);
            return RedirectToAction("List");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            ProductDto productDto = productService.GetProduct(id);
            EditProductViewModel editProductViewModel = Mapper.Map<ProductDto, EditProductViewModel>(productDto);
            return View(editProductViewModel);
        }

        [HttpPost]
        public ActionResult Edit(EditProductViewModel editProductViewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(editProductViewModel);
            }

            ProductDto productDto = Mapper.Map<EditProductViewModel, ProductDto>(editProductViewModel);
            productService.EditProduct(productDto);
            return RedirectToAction("List");
        }

        [HttpGet]
        public ActionResult List()
        {
            List<ProductDto> productListDto = productService.GetProductList();
            List<ProductListViewModel> productListViewModel = Mapper.Map<List<ProductDto>, List<ProductListViewModel>>(productListDto);
            return View(productListViewModel);
        }

        [HttpGet]
        public ActionResult Details(int id)
        {
            ProductDto productDto = productService.GetProduct(id);
            ProductDetailsViewModel productDetailsViewModel = Mapper.Map<ProductDto, ProductDetailsViewModel>(productDto);
            return View(productDetailsViewModel);
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            productService.DeleteProduct(id);
            return RedirectToAction("List");
        }
    }
}