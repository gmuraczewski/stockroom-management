﻿using StockroomManagement.DataAccess.Models;

namespace StockroomManagement.DataAccess.Interfaces
{
    public interface IRepositories
    {
        IRepository<Product> Products { get; }

        IRepository<Storage> Storages { get; }

        IRepository<Availability> Availabilities { get; }

        void Save();
    }
}
