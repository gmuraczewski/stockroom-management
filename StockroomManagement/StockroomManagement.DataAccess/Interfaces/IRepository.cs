﻿using System.Linq;

namespace StockroomManagement.DataAccess.Interfaces
{
    public interface IRepository<TEntity>
    {
        IQueryable<TEntity> Query();

        void Add(TEntity item);

        void Update(TEntity item);

        void Delete(TEntity item);
    }
}
