﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace StockroomManagement.DataAccess.Models
{
    public class Product
    {
        [Key]
        public int ID { get; set; }

        [Required]
        [MinLength(3)]
        public string Name { get; set; }

        [Required]
        public double Price { get; set; }

        public string Description { get; set; }

        public virtual ICollection<Availability> Availability { get; set; }
    }
}
