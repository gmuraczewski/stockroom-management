﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace StockroomManagement.DataAccess.Models
{
    public class Storage
    {
        [Key]
        public int ID { get; set; }

        [Required]
        [MinLength(3, ErrorMessage = "Nazwa musi mieć co najmniej 3 znaki")]
        public string Name { get; set; }

        [Required]
        [MinLength(3, ErrorMessage = "Miasto musi mieć co najmniej 3 znaki")]
        public string City { get; set; }

        public virtual ICollection<Availability> Availability { get; set; }
    }
}
