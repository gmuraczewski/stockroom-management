﻿using System.ComponentModel.DataAnnotations;

namespace StockroomManagement.DataAccess.Models
{
    public class Availability
    {
        [Key]
        public int ID { get; set; }

        public int ProductID { get; set; }

        public int StorageID { get; set; }

        [Required]
        [Range(0, 100, ErrorMessage = "Ilośc sztuk może być z zakresu 0-100")]
        public int Quantity { get; set; }

        public virtual Product Product { get; set; }

        public virtual Storage Storage { get; set; }
    }
}
