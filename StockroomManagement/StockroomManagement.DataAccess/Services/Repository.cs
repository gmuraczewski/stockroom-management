﻿using StockroomManagement.DataAccess.Interfaces;
using System.Data.Entity;
using System.Linq;

namespace StockroomManagement.DataAccess.Services
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private readonly StockroomContext context;
        private DbSet<TEntity> dbSet;

        public Repository(StockroomContext context)
        {
            this.context = context;
            this.dbSet = context.Set<TEntity>();
        }

        public void Add(TEntity item)
        {
            dbSet.Add(item);
            context.SaveChanges();                
        }

        public void Delete(TEntity item)
        {
            dbSet.Attach(item);
            dbSet.Remove(item);
            context.SaveChanges();
        }

        public void Update(TEntity item)
        {
            context.Entry(item).State = EntityState.Modified;
            context.SaveChanges();
        }

        public IQueryable<TEntity> Query()
        {
            return dbSet.AsQueryable();
        }
    }
}
