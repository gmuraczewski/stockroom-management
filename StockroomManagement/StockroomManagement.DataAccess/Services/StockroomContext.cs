﻿using StockroomManagement.DataAccess.Models;
using System.Data.Entity;

namespace StockroomManagement.DataAccess.Services
{
    public class StockroomContext : DbContext
    {
        public StockroomContext() : base("StockroomContext")
        {
        }

        public virtual DbSet<Availability> Availabilities { get; set; }

        public virtual DbSet<Product> Products { get; set; }

        public virtual DbSet<Storage> Storages { get; set; }
    }
}
