﻿using StockroomManagement.DataAccess.Interfaces;
using StockroomManagement.DataAccess.Models;

namespace StockroomManagement.DataAccess.Services
{
    public class Repositories : IRepositories
    {
        private StockroomContext context;
        
        public Repositories(StockroomContext context)
        {
            this.context = context;
            Products = new Repository<Product>(context);
            Storages = new Repository<Storage>(context);
            Availabilities = new Repository<Availability>(context);
        }

        public IRepository<Product> Products { get; }

        public IRepository<Storage> Storages { get; }

        public IRepository<Availability> Availabilities { get; }

        public void Save()
        {
            context.SaveChanges();
        }
    }
}
